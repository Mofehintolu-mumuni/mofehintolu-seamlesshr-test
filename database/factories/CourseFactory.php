<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {

    //define factory elements
    $code = rand(1,10);
    $courseTitle = $faker->name;
    $courseCode = $courseTitle.$code;

    return [
        'text' => $faker->name,
        'Title' => $courseTitle,
        'Code' => $courseTitle."-".$code,
        'Teacher' => $faker->name,
    ];
});
