

## Course-Ware-App API usage Documentation

The Seamless Hiring Interview Task was to implement the following:

-  Create an endpoint for User login and registration. Implement JWT.

-  Add 3 more self-defined columns to the course table, with another migration.

-  Create an endpoint to call a course factory to create 50 courses. We want you to queue this operation in a job.

-  Create an endpoint for a user to register in one or more courses. 

-  Create an endpoint to see list of all courses. If the user is registered in a course, return the date enrolled in that course.

-  Create an endpoint to export all courses with the Maatexcel (any version) package in excel or csv format.


All features were implemented according to requirements given.


## APPLICATION SETUP

RUN THE FOLLOWING COMMANDS BEFORE TESTING THE APPLICATION

-  composer install

-  php artisan migrate

-  php artisan serve

-  Please run php artisan queue:work before visiting the route below

    METHOD:GET
    Route::get('/factory','API\CORE\FactoryController@CourseTableSeeder');


The following Routes can be used to test each task requirement given;

    TASK

-  Create an endpoint for User login and registration. Implement JWT.

    RESULTS

N/B: The register method has verify email feature which sends a token via mail
      to use this feature mailtrap.io needs to be set up and sendMail parameter must be
      provided in the form body and set to 'yes'.

    if verify email feature is not desired to test or mailtrap.io account is not set up,
     please do not set sendMail in the form body as a parameter


 METHOD:POST, BODY PARAMETER: {email},{password},{name}, optional {sendMail}
 Route::post('/register','API\AUTH\RegisterController@register');

METHOD:POST, BODY PARAMETER: {email},{password}
Route::post('/login','API\AUTH\LoginController@login');

METHOD:POST, BODY PARAMETER {email}
Route::post('/forgot/password','API\AUTH\ForgotPasswordController@forgotPassword');

METHOD:GET, PARAMETER: {token}
Route::get('/forgot/password/checkResetToken/{token}','API\AUTH\ResetPasswordController@checkToken');

METHOD:PUT , BODY PARAMETER: {password},{password_confirmation},{token}
Route::put('/forgot/password/reset','API\AUTH\ResetPasswordController@resetPassword');

METHOD:GET, PARAMETER: {token}
Route::get('/users/email/verification/{token}','API\AUTH\UserEmailVerificationController@verifyEmail');

METHOD:GET , MIDDLEWARE: auth:api , AUTH TOKEN is needed to call this route
Route::get('/logout','API\AUTH\LogoutController@logout');


METHOD:GET , MIDDLEWARE: auth:api , AUTH TOKEN is needed to call this route
Route::get('/profile','API\CORE\profileViewController@showProfile');




TASK

-  Add 3 more self-defined columns to the course table, with another migration.

RESULT

Migration created: 2020_02_02_222205_update_courses_table.php






TASK

-  Create an endpoint to call a course factory to create 50 courses. We want you to queue this operation in a job.

RESULT

N/B: Please run php artisan queue:work before visiting the route below

METHOD:GET
Route::get('/factory','API\CORE\FactoryController@CourseTableSeeder');





TASK

-  Create an endpoint for a user to register in one or more courses. 

RESULTS

    METHOD:POST , PARAMETER: {courseId}, MIDDLEWARE: auth:api , AUTH TOKEN is needed in header to call this route

    Route::post('/courses/register','API\CORE\CoursesController@registerInCourse');





TASK

-  Create an endpoint to see list of all courses. If the user is registered in a course, return the date enrolled in that course.

RESULT

METHOD:GET , MIDDLEWARE: auth:api , AUTH TOKEN is needed in header to call this route
Route::get('/courses/getall','API\CORE\CoursesController@showAllCoursesAndEnrolledDates');







TASK

-  Create an endpoint to export all courses with the Maatexcel (any version) package in excel or csv format.

RESULT

METHOD:POST , BODY PARAMETER: {file_type} options xlsx or csv

Route::post('courses/export','API\CORE\CoursesController@exportCourses');




## CONCLUSION


The task given were all completed successfully. The authentication system developed is complete with register,login, logout,verify email, reset password, and view profile functionalities. Event and Listeners were used for the email sending functionality.
 In the Login, register and logout controllers that involve token creation and deletion respectively, the process of token creation was handled by generateToken(string $email, string $password) and revokeToken() provided by the tokenManager interface. 
 This was done in-line with SOLID principles of software development and to aid easy replacement of the auth server to laravel passport if neccessary. The jwtImplementation.php provides a concrete class implementation of the tokenManager interface and was called in the tokenManagerServiceProvider.

N/B: The register method has verify email feature which sends a token via mail
      to use this feature mailtrap.io needs to be set up and sendMail parameter must be
      provided in the form body and set to 'yes'.

    if verify email feature is not desired to test or mailtrap.io account is not set up,
     please do not set sendMail in the form body as a parameter


 METHOD:POST, BODY PARAMETER: {email},{password},{name}, optional {sendMail}
 Route::post('/register','API\AUTH\RegisterController@register');




N/B: All the routes below require a valid mailtrap.io account configuration in .env before they would work properly and send mails

METHOD:GET, PARAMETER: {token}
Route::get('/users/email/verification/{token}','API\AUTH\UserEmailVerificationController@verifyEmail');


METHOD:POST, BODY PARAMETER {email}
Route::post('/forgot/password','API\AUTH\ForgotPasswordController@forgotPassword');
