<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
| Route::middleware('auth:api')->get('/user', function (Request $request) {
|    return $request->user();
|    });
|
|
*/




Route::group(['middleware' => ['auth:api','jwt_auth']], function () {

    Route::get('/profile','API\CORE\profileViewController@showProfile');

    Route::get('/logout','API\AUTH\LogoutController@logout');

    //POST PARAM {courseId}
    Route::post('/courses/register','API\CORE\CoursesController@registerInCourse');

    Route::get('/courses/getall','API\CORE\CoursesController@showAllCoursesAndEnrolledDates');

});



//run courses creation
Route::get('/factory','API\CORE\FactoryController@CourseTableSeeder');

//Initial authentication routes

 //POST PARAM {email},{password},{name}
Route::post('/register','API\AUTH\RegisterController@register');

//POST PARAM {email},{password}
Route::post('/login','API\AUTH\LoginController@login');

//POST PARAM {email}
Route::post('/forgot/password','API\AUTH\ForgotPasswordController@forgotPassword');


Route::get('/forgot/password/checkResetToken/{token}','API\AUTH\ResetPasswordController@checkToken');

//PUT PARAM {password},{password_confirmation},{token}
Route::put('/forgot/password/reset','API\AUTH\ResetPasswordController@resetPassword');


Route::get('/users/email/verification/{token}','API\AUTH\UserEmailVerificationController@verifyEmail');

//Export all courses to excel format
//POST PARAM {file_type} options xlsx or csv
Route::post('courses/export','API\CORE\CoursesController@exportCourses');


//Fallback route incase an invalid route is called

Route::fallback(function()
{
    $response = 'URL DOES NOT EXIST';
    return response()->json($response, 404);
});
