<?php

namespace App\Exports;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;

class coursesExport extends Controller implements FromCollection, WithHeadings
{
    use Exportable;

    public function collection()
    {
        return Course::all();
    }

    //Add excel spread sheet headings
    public function headings(): array
    {
        return [
            'Course ID',
            'Text/Description',
            'Title',
            'Date Created',
            'Date Updated',
            'Course Code',
            'Course Teacher',
        ];
    }

}
