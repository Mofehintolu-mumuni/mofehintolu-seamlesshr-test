<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class courseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'Course Name' => $this->Title,
            'Course Code' => $this->Code,
            'Course Teacher' => $this->Teacher,
            'Course Description' => $this->text,
            'Date Enrolled' => ($this->date_of_registration === null) ? "Not Enrolled" : $this->date_of_registration,
        ];
    }
}
