<?php

namespace App\Http\Controllers\API\CORE;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\API\AUTH\JsonResponseController;



class profileViewController extends Controller
{
    protected $response;

    public function __construct(JsonResponseController $response)
    {
        $this->response = $response;
    }


    /**
    * SHOW USER PROFILE DETAILS
    * @return json
    */

    public function showProfile()
    {

        try{

           $userDetails = auth('api')->user();

           $payload = ['status' => 'success','data' => $userDetails,'links'=> null];

            $httpStatusCode = 200;

            return $this->response->JsonResponse($payload,$httpStatusCode);

        }catch(\Throwable $e)
        {
            $payload = ['status' => 'error','data' => $e->getMessage(),'links'=> null];

            $httpStatusCode = 200;

            //log show user profile
            Log::error("show user profile error ".$e->getMessage());

            return $this->response->JsonResponse($payload,$httpStatusCode);
        }


    }
}
