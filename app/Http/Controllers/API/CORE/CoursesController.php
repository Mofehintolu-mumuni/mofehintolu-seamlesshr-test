<?php

namespace App\Http\Controllers\API\CORE;

use App\User;
use App\Course;
use App\registered_course;
use Illuminate\Http\Request;
use App\Exports\coursesExport;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\courseResource;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\JsonResponseController;

class CoursesController extends Controller
{

    protected $response;

    public function __construct(JsonResponseController $response)
    {
        $this->response = $response;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'courseId' => ['required', 'string', 'max:255'],
        ]);
    }

    /**
    * Define registerInCourse functionality
    * Check if a user is registered in a course if not
    * enroll a user in course
    * @param Request object
    * @param registered_course object
    * @return json response
    *
    */

    public function registerInCourse(Request $request,registered_course $registerCourse)
    {

         //validate request values
         $validator = $this->validator(['courseId' => $request->courseId]);

         if ($validator->fails())
         {
             //log Login validation errors
             Log::error("registerInCourse validation error ");

             $payload = ['status' => 'error','data' => $validator->errors()->all(),'links'=> null];

             $httpStatusCode = 422;

            return $this->response->JsonResponse($payload,$httpStatusCode);
         }


        if(null != $request->courseId)
        {
            try{

                //check if course exists
                $checkCourse = Course::where('id',$request->courseId)
                                            ->first();

            if(null == $checkCourse)
            {
                //log error
                Log::error("Course selected does not exist ");

                $payload = ['status' => 'error','data' => 'Course selected does not exist ','links'=> null];

                $httpStatusCode = 400;

                 return $this->response->JsonResponse($payload,$httpStatusCode);
            }


            //check if user has registered for course before
            $result = registered_course::where('course_id',$request->courseId)
                                        ->where('user_id',auth('api')->user()->id)
                                        ->first();

            if(null == $result)
            {
                        // return response()->json(["data" => "user id is : ".auth('api')->user()->id],200);
                    //register course if user has not registered before
                    $registerCourse->user_id = auth('api')->user()->id;
                    $registerCourse->course_id = $request->courseId;
                    $registerCourse->enrollment_status = true;
                    $registerCourse->save();

                    if($registerCourse)
                    {
                         //log success
                         Log::info("Course registered successfully ");

                        $payload = ['status' => 'success','data' => 'Course registered successfully','links'=> null];

                        $httpStatusCode = 200;

                         return $this->response->JsonResponse($payload,$httpStatusCode);
                    }else{

                        //log error
                        Log::error("Course registration not successful ");

                        $payload = ['status' => 'error','data' => 'Course registration not successful','links'=> null];

                        $httpStatusCode = 400;

                         return $this->response->JsonResponse($payload,$httpStatusCode);
                    }
            }else{

                  //log error
                Log::error("You have already registered for this Course ");

                $payload = ['status' => 'error','data' => 'You have already registered for this Course','links'=> null];

                $httpStatusCode = 400;

                 return $this->response->JsonResponse($payload,$httpStatusCode);
            }

                }catch(\Throwable $e){

                    //log error
                    Log::error("Course registration not successful ".$e->getMessage());

                         $payload = ['status' => 'error','data' => 'Course registration not successful: '.$e->getMessage() ,'links'=> null];

                        $httpStatusCode = 400;

                         return $this->response->JsonResponse($payload,$httpStatusCode);
                }

        }
        else{

            //log error
            Log::error("Course ID not provided ");

            $payload = ['status' => 'error','data' => 'Course ID not provided','links'=> null];

            $httpStatusCode = 400;

           return $this->response->JsonResponse($payload,$httpStatusCode);
        }

    }


    /**
    * Define showAllCoursesAndEnrolledDates functionality
    * List of all courses. If the user is registered in a course,
    * return the date enrolled in that course
    * @return json response
    *
    */

    public function showAllCoursesAndEnrolledDates()
    {

        try{

            //get user ID
            $user_id = auth('api')->user()->id;

           /* return courseResource::collection(DB::table('courses')
            ->leftjoin('registered_courses', function ($join) use ($user_id) {
                $join->on('courses.id', '=', 'registered_courses.course_id')
                ->where('user_id', $user_id);
                })
            ->get());
            */

                //pass data returned into collection and format response
            $data = courseResource::collection(DB::table('courses')
            ->leftjoin('registered_courses', function ($join) use ($user_id) {
                $join->on('courses.id', '=', 'registered_courses.course_id')
                ->where('user_id', $user_id);
                })
            ->get());

            //check if data is null and act on it
            if(null != $data)
            {
                  //log success
                  Log::info("Courses data generated successfully ");

                  $payload = ['status' => 'success','data' => $data->all(),'links'=> null];

                  $httpStatusCode = 200;

                   return $this->response->JsonResponse($payload,$httpStatusCode);
            }
            else{
               //log success
               Log::info("No courses found ");

               $payload = ['status' => 'success','data' => 'No courses found','links'=> null];

               $httpStatusCode = 200;

                return $this->response->JsonResponse($payload,$httpStatusCode);
            }

        }
        catch(\Throwable $e)
        {
            //get error message from error

                //log success
                Log::error("Courses data not generated successfully ".$e->getMessage());

                $payload = ['status' => 'error','data' => 'Courses data not generated successfully','links'=> null];

                $httpStatusCode = 400;

                 return $this->response->JsonResponse($payload,$httpStatusCode);
        }


    }




    public function exportCourses(Request $request)
    {
        try{

            $file_type = strtolower($request->file_type);

            if(($file_type == 'csv') || ($file_type == 'xlsx')){

                //handle xlsx format export
                if($file_type == 'xlsx'){

                    \Excel::store(new coursesExport(2018), 'courses'.time().'.xlsx');

                    //log success
                Log::info("Courses data exported successfully in xlsx format");

                $payload = ['status' => 'success','data' => 'Courses data exported successfully in xlsx format','links'=> null];

                $httpStatusCode = 200;

                return $this->response->JsonResponse($payload,$httpStatusCode);
                }

                //handle csv format export
                if($file_type == 'csv'){

                    \Excel::store(new coursesExport(2018), 'courses'.time().'.csv');

                    //log success
                Log::info("Courses data exported successfully in csv format");

                $payload = ['status' => 'success','data' => 'Courses data exported successfully in csv format','links'=> null];

                $httpStatusCode = 200;

                return $this->response->JsonResponse($payload,$httpStatusCode);
                }


            }else{
                 //log error
               Log::error("Invalid filetype provided enter xlsx or csv formats only");

               $payload = ['status' => 'error','data' => 'Invalid filetype provided enter xlsx or csv formats only','links'=> null];

               $httpStatusCode = 200;

                return $this->response->JsonResponse($payload,$httpStatusCode);
            }

      //  ob_end_clean();
       // return (new coursesExport)->download('courses.xlsx', \Maatwebsite\Excel\Excel::XLSX,['Content-Type' => 'application/vnd.ms-excel']);

            }catch(\Throwable $e){
                 //log error
               Log::error("Courses data not exported successfully ".$e->getMessage());

               $payload = ['status' => 'error','data' => 'Courses data not exported successfully','links'=> null];

               $httpStatusCode = 400;

                return $this->response->JsonResponse($payload,$httpStatusCode);
            }
    }




}
