<?php

namespace App\Http\Controllers\API\CORE;

use Illuminate\Http\Request;
use App\Jobs\courseSeederJob;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class FactoryController extends Controller
{


     /**
    * Define CourseTableSeeder functionality
    * Queue seeding of courses table using CourseSeeder
    * by dispatching a job
    *
    * @return string
    *
    */

    public function CourseTableSeeder(): string
    {
        try{
            //define command
            $command = 'db:seed';

            $commandInstructions = ['command' => $command, 'option' => ['--class'=>'CourseSeeder']];

            //pass params to job for handling
            courseSeederJob::dispatch($commandInstructions);

            //log queue success
            Log::info('Database course seeding queued successfully');

            //return response to user while job is handled by workers
            return response()->json(['result' => "Database course seeding queued successfully"],200);
        }catch(\Throwable $e)
        {
            //log queue error
            Log::error('Database course seeding queued not successfully'.$e->getMessage());

            return response()->json(['result' => "Database course seeding queued not successfully"],422);
        }

    }


}
