<?php

namespace App\Http\Controllers\API\AUTH;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;
use App\Http\Controllers\API\AUTH\JsonResponseController;

class JsonResponseController extends Controller
{
    /**
     * Get json response parameters.
     * and return appropriate response
     *
     * @param array $payload
     * @param int $httpStatusCode
     * @param array $headers
     *
     * @return json
     */
    //declare compulsory headers to return
    use ReturnHeaders;

    public function JsonResponse(array $payload,int $httpStatusCode,array $headers = null)
    {
        $headers == null ? $headers = [] : $headers;
        try{
        //recieve params and get necessary values from them
        $payloadKeys = array_keys($payload);
        $headerKeys = array_keys($headers);

        $status = $payload[$payloadKeys[0]];
        $data = $payload[$payloadKeys[1]];
        $links = $payload[$payloadKeys[2]];

        $responsePayload = [
                            'status' => $status,
                            'data' => $data,
                            'links' => $links
                            ];

        //Pass a pointer to the headers array using &$returnHeaders as the first param in $closureParams
        //This is to enable the original copy of the array to be altered in the array_map function
        $closureParams = [&$this->returnHeaders,$headers];

        array_map(function($value)use($closureParams)
                {
                $closureParams[0][$value] = $closureParams[1][$value];
                },$headerKeys);
        return response()->json($responsePayload,$httpStatusCode)->withHeaders($this->returnHeaders);
        }
        catch(Exception $e)
        {
        $errorMessage = $e->getMessage();
        $errorPayload = ['status'=> 'error', 'data' => $errorMessage, 'links'=> null];
        return response()->json($errorPayload,400)->withHeaders($this->returnHeaders);

        }

    }
}
