<?php

namespace App\Http\Controllers\API\AUTH;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Interfaces\tokenManager;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;

class LogoutController extends Controller
{
    protected $tokenManager;

    function __construct(tokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }


    //prepare custom response headers to return
    use ReturnHeaders;

    /**
    * Define logout functionality
    * @param Request object
    * @param User object
    *
    * @return json response
    *
    */

    public function logout(Request $request,User $user)
    {
       try{
       $booleanResponse = $this->tokenManager->revokeToken();

       if($booleanResponse)
       {
        $message = 'You have been succesfully logged out!';

        $response = ['status' => 'success','message' => $message];

        //log Logout successful message
        Log::info("You have been succesfully logged out");

        return response()->json($response, 200)->withHeaders($this->returnHeaders);

       }else{
        $message = 'Log out not succesful, please try again';

        $response = ['status' => 'error','message' => $message];

        //log Logout errors
        Log::error("Log out not succesful");

        return response()->json($response, 200)->withHeaders($this->returnHeaders);
       }


    }
    catch(Exception $e)
    {
     $message = 'An error occurred while trying to log out! '.$e->getMessage();

     $response = ['status' => 'error','message' => $message];

    //log Logout errors
     Log::error("Log out not succesful ".$e->getMessage());

     return response()->json($response, 422)->withHeaders($this->returnHeaders);
    }

    }
}
