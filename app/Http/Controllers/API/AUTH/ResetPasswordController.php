<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;

class ResetPasswordController extends Controller
{
     //prepare custom response headers to return
     use ReturnHeaders;

      //make validation rules

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'token' => ['required', 'string', 'alpha_num', 'max:255']
        ]);
    }

    protected function validatorReset(array $data)
    {
        return Validator::make($data, [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'token' => ['required', 'string', 'alpha_num', 'max:255']
        ]);
    }

     /**
    *
    * @params Request $request object
    * @params User $user object
    * @returns json
    *
    */

    public function checkToken(Request $request,User $user)
    {
        $validator = $this->validator(['token' => $request->token]);

        if($validator->fails())
        {
            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        $user = User::where('password_reset_token',$request->token)->first();

        if($user)
        {
            //send success response
            $response = ['status' => 'success','message' => 'Password can now be reset','resetToken' => $request->token];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
        else
        {
            //send error response
            $response = ['status' => 'error','message' => 'Token invalid'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }


    }

    /**
    *
    * @params Request $request object
    * @params User $user object
    * @returns json
    *
    */

    public function resetPassword(Request $request,User $user)
    {
        $validator = $this->validatorReset(['password' => $request->password,'password_confirmation' => $request->password_confirmation,'token' => $request->token]);

        if($validator->fails())
        {
            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        $user = User::where('password_reset_token',$request->token)->first();

        if($user)
        {

            $user->password = Hash::make($request->password, ['rounds' => 12]);
            $user->password_reset_token = null;
            $user->save();

            //send success response
            $response = ['status' => 'success','message' => 'Password reset successful, you can login'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
        else
        {
            //send error response
            $response = ['status' => 'error','message' => 'Password reset not successful'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
    }


}
