<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\PasswordResetNotification;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;


class ForgotPasswordController extends Controller
{


    //prepare custom response headers to return
    use ReturnHeaders;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    //make validation rules

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255']
        ]);
    }

    /**
    * @param Request $request object
    * @param User $user object
    *
    * @return json
    */


    public function forgotPassword(Request $request, User $user)
    {

        $validator = $this->validator(['email' => $request->input('email')]);

        if ($validator->fails())
        {
            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        //check for presence of email provided in database and send password
        //reset email

        $user = User::where('email', strtolower($request->input('email')))->first();

        if($user)
        {
            //generate user password reset is and raise mail send event
            $passwordResetToken = md5(rand(1,1000000000).now().$request->input('email'));

            $user->password_reset_token = $passwordResetToken;

            $user->save();

            //raise email send event
            event(new PasswordResetNotification($request->input('email'),$passwordResetToken));

            //send response
            $response = ['status' => 'success','message' => 'Password reset mail sent successfuly'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
        else
        {
            //send error response
            $response = ['status' => 'error','message' => 'Invalid email provided'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }





    }




}
