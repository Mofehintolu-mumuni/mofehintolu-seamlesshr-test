<?php

namespace App\Http\Controllers\API\AUTH;


use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;
use App\Http\Controllers\API\AUTH\Traits\SendAccountVerificationMail;

class AuthResendEmailVerificationLinkController extends Controller
{
    use ReturnHeaders,SendAccountVerificationMail;


    public function resendEmailVerifyLink(Request $request)
    {
        $UserDetails = $request->user()->toArray();
        $user = User::where('email',strtolower($UserDetails['email']))->first();

        if($user)
        {
            if($user->verify_email_status === 'VERIFIED')
            {
                $response = ['status' => 'error','VerifyMailSentStatus' => true, 'message' => 'User already verified'];

                return response()->json($response, 200)->withHeaders($this->returnHeaders);
            }
            else
            {
                //send register event()
                 $mailResponse = $this->sendVerificationMail($user,$UserDetails['email']);

                 $response = ['status' => 'success','VerifyMailSentStatus' => $mailResponse,'message' => 'User verification mail re-sent successfuly'];

                 return response()->json($response, 200)->withHeaders($this->returnHeaders);
            }
        }
        else
        {
            $response = ['status' => 'error','VerifyMailSentStatus' => false,'message' => 'User verification status unknown'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }

    }
}
