<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Http\Interfaces\tokenManager;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;

class LoginController extends Controller
{
    protected $tokenManager;

    function __construct(tokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    //prepare custom response headers to return
    use ReturnHeaders;

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

 /**
    * Define login functionality
    * @param Request object
    * @return json response
    *
    */

    public function login (Request $request) {

        //validate request values
        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            //log Login validation errors
            Log::error("Login validation error ");

            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        try{
            //check if user has record
        $user = User::where('email', strtolower($request->email))->first();

        if ($user) {
        //check if user password is valid
            if (Hash::check($request->password, $user->password)) {

                //get access token from $this->tokenManager
               $tokenResponseArray = $this->tokenManager->generateToken($user->email,$request->password);

               if($tokenResponseArray['status'])
               {
                   $token = $tokenResponseArray['token'];

                   $response = ['status' => 'success','token' => $token,'message' => 'Login successful'];

                   //log error
                   Log::info("Login successful for user ".$user->id);

               return response()->json($response, 200)->withHeaders($this->returnHeaders);

               }else{

                   //log error
                   Log::error("Login not successful token generation error for user ".$user->id);

                $response =  ['status' => 'error','token' => null, 'message' => 'Login not successful please try again'];

                return response()->json($response, 200)->withHeaders($this->returnHeaders);
               }



            } else {
                 //log error
                 Log::error("Login not successful Password or email missmatch please try again");

                $response =  ['status' => 'error','token' => null, 'message' => 'Password or email missmatch'];
                return response()->json($response, 200)->withHeaders($this->returnHeaders);
            }

        } else {
                //log error
                Log::error("Login not successful Password or email missmatch please try again");

                $response =  ['status' => 'error','token' => null, 'message' => 'Password or email missmatch please try again'];
                return response()->json($response, 200)->withHeaders($this->returnHeaders);

        }


            }catch(\Throwable $e)
                {

                    Log::error("Login not successful ".$e->getMessage());

                    $response =  ['status' => 'error','token' => null, 'message' => 'Login not successful'];

                    return response()->json($response, 200)->withHeaders($this->returnHeaders);
                }


    }

}
