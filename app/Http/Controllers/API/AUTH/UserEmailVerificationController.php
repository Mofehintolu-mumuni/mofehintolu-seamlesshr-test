<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Http\Request;
use App\Events\VerifiedMessage;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;

class UserEmailVerificationController extends Controller
{
    use ReturnHeaders;

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'token' => ['required', 'string', 'alpha_num', 'max:255']
        ]);
    }


    /**
    * @param email verification $token
    * @return json
    */

    public function verifyEmail($token)
    {
        $validator = $this->validator(['token' => $token]);

        if($validator->fails())
        {
            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);

        }

        $user = User::where('verify_email_token',$token)->first();

        if($user)
        {
             //remove verification token
            $userEmail = $user->email;
            $user->verify_email_token = null;
            $user->email_verified_at = now();
            $user->verify_email_status = 'VERIFIED';
            $user->save();
            //raise verification event
            event(new VerifiedMessage($userEmail));

            $response = ['status' => 'success','VerifyStatus' => 'VERIFIED'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
        else
        {

            $response = ['status' => 'error','VerifyStatus' => 'NOT VERIFIED'];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }


    }
}
