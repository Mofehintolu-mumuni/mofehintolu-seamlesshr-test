<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Interfaces\tokenManager;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\AUTH\Traits\ReturnHeaders;
use App\Http\Controllers\API\AUTH\Traits\SendAccountVerificationMail;



class RegisterController extends Controller
{
    //prepare custom response headers to return
    use ReturnHeaders,SendAccountVerificationMail;

    protected $tokenManager;

    function __construct(tokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    //make validation rules

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'name' => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user and return access tokens.
     * @param Request $request
     * @param  User as $user
     * @return json access token on success
     */

    public function register (Request $request,User $user) {

        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            //log registration validation errors
            Log::error("Registration validation error");

            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        $oldPassword = $request->password;

        $request['password'] = Hash::make($request['password'], ['rounds' => 12]);

        //trap mail send value if provided

        $mailSendingStatus = $request->sendMail;

        $userDetailsArray = $request->toArray();


        //$userDetailsArray['verify_email_token'] = md5(rand(1,100000).now().$userDetailsArray['email']);
        try{
            $user->password = $userDetailsArray['password'];
            $user->email = strtolower($userDetailsArray['email']);
            $user->name = ucfirst($userDetailsArray['name']);

            $user->save();
        }catch(\Throwable $e){
            $response = ['status' => 'error1','token' => $e->getMessage(),'VerifyMailSentStatus' => false];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }


        if($user)
        {
            //get access token from $this->tokenManager
            $tokenResponseArray = $this->tokenManager->generateToken($user->email,$oldPassword);

            if($tokenResponseArray['status'])
            {
                $token = $tokenResponseArray['token'];

                if(null == $mailSendingStatus)
                {
                    $mailResponse = false;
                }
                else if((null != $mailSendingStatus) && (strtolower($mailSendingStatus) == "yes")){
                     //send register event mail()
                    $mailResponse = $this->sendVerificationMail($user,strtolower($userDetailsArray['email']));
                }
                else if((null != $mailSendingStatus) && (strtolower($mailSendingStatus) != "yes")){
                    $mailResponse = false;
                }


                $response = ['status' => 'success','token' => $token,'VerifyMailSentStatus' => $mailResponse];

                //log registration successful message
                Log::info("Registration successful for user ".$user->email);

                return response()->json($response, 200)->withHeaders($this->returnHeaders);
            }else{

                //log error
                Log::error("Registration token generation error");

                $response = ['status' => 'error','token' => null,'VerifyMailSentStatus' => false];

                return response()->json($response, 200)->withHeaders($this->returnHeaders);
            }


        }
        else{
            $response = ['status' => 'error','token' => null,'VerifyMailSentStatus' => false];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }



        }


}
