<?php

namespace App\Http\Controllers\API\AUTH\Traits;

use App\User;
use App\Events\EmailVerification;

trait SendAccountVerificationMail
{
    /**
     * @param $user instance of User model
     *
     * @param $email user email
     *
     * @return bool
     */

    protected function sendVerificationMail($user,$email)
    {
        $emailVerificationToken = md5(rand(1,100000).now().$email);
        $user->verify_email_token = $emailVerificationToken;
        $user->save();

        if($user)
        {
            //raise verify email event
            event(new EmailVerification($email,$emailVerificationToken));

            //send return value to waiting function
            return true;
        }
        else
        {
            return false;
        }



    }


}
