<?php

namespace App\Http\Controllers\API\AUTH\Traits;

trait ReturnHeaders
{
    /**
     * prepare custom response headers to return
     *
     */
    public $returnHeaders = ['Content-Type' => 'application/json',
                    'X-DEVELOPER' => 'MOFEHINTOLU MUMUNI',
                    'X-DEVELOPER-WEBSITE' => 'MOFEHINTOLUMUMUNI.COM',
                    'X-DEVELOPER-EMAIL' => 'HELLO@MOFEHINTOLUMUMUNI.COM'
                    ];
}
