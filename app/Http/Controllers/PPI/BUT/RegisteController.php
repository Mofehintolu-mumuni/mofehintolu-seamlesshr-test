<?php

namespace App\Http\Controllers\API\AUTH;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Exports\coursesExport;

class RegisterController extends Controller
{

    public $returnHeaders = ['Content-Type' => 'application/json',
                    'X-DEVELOPER' => 'MOFEHINTOLU MUMUNI',
                    'X-DEVELOPER-WEBSITE' => 'MOFEHINTOLUMUMUNI.COM',
                    'X-DEVELOPER-EMAIL' => 'HELLO@MOFEHINTOLUMUMUNI.COM'
                    ];


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }


    /**
     * Create a new user and return access tokens.
     * @param Request $request
     * @param  User as $user
     * @return json access token on success
     */

    public function register (Request $request,User $user) {

        $validator = $this->validator($request->all());

        if ($validator->fails())
        {
            return response(['status' => 'error','errors'=>$validator->errors()->all()], 422);
        }

        $oldPassword = $request->password;

        $request['password'] = Hash::make($request['password'], ['rounds' => 12]);

        $userDetailsArray = $request->toArray();

        //$userDetailsArray['verify_email_token'] = md5(rand(1,100000).now().$userDetailsArray['email']);

        //$user = User::create($userDetailsArray);
        $user->password = $userDetailsArray['password'];
        $user->email = strtolower($userDetailsArray['email']);
        $user->name = ucfirst($userDetailsArray['name']);

        //$user->verify_email_token = $userDetailsArray['verify_email_token'];
        $user->save();

        if($user)
        {

            $credentials = ['email' => $userDetailsArray['email'],'password' => $oldPassword];

            $token = auth('api')->attempt($credentials);

           $mailResponse = true;
            $response = ['status' => 'success','token' => $token,'VerifyMailSentStatus' => $mailResponse];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }
        else{
            $response = ['status' => 'error','token' => null,'VerifyMailSentStatus' => false];

            return response()->json($response, 200)->withHeaders($this->returnHeaders);
        }



        }




 /**
    * Get a JWT via given credentials.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth('api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth('api')->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function download()
    {
        \Excel::store(new coursesExport(2018), 'courses.xlsx');
        //return Excel::download(new coursesExport, 'courses.xlsx');
        ob_end_clean();
        return (new coursesExport)->download('courses.xlsx', \Maatwebsite\Excel\Excel::XLSX,['Content-Type' => 'application/vnd.ms-excel']);
        /*$fileName = 'trial.xlsx';
        $filePath = public_path('excels/').$fileName;
        $headers = ['Content-Type' => 'application/vnd.ms-excel','Content-Disposition' => "attachment; filename='Report.xlsx'"];
       // ob_end_clean();
        return response()->download($filePath,"new.xlsx",$headers);
        */
    }



}
