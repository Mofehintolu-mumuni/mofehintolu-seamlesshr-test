<?php

namespace App\Http\InterfaceImplementations\TokenManagerImplementations;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Interfaces\tokenManager;

class jwtImplementation implements tokenManager
{

    /**
    * Define generateToken functionality
    * @param email string
    * @param password string
    *
    * @return token string
    *
    */
    function generateToken(string $email, string $password):array
    {
        try{

            if(($email != null) && ($password != null))
            {
                $credentials = ['email' => $email,'password' => $password];
                //get token from JWT Driver
                $token = auth('api')->attempt($credentials);

                //log token generation success
                Log::info("Token successfully generated for user ".auth('api')->user()->id);

                return [
                    'status' => true,
                    'token' =>  $token
                ];

            }else{

                 //log token generation error
                 Log::error("Token generation failed due to empty email or password");

                return [
                    'status' => false,
                    'token' => null
                ];
            }


        }
        catch(\Throwable $e){

            //log token generation error
            Log::error("Token generation failed for user message: ".$e->getMessage());

            return [
                'status' => false,
                'token' => null
            ];

        }
    }






    /**
    * Define revokeToken functionality
    *
    * @return boolean boolean
    *
    */
    function revokeToken()
    {
        try{

              //log token generation success
              Log::info("Token revoking for user ".auth('api')->user()->id);

              auth('api')->logout();

            return true;

        }catch(\Throwable $e){

             //log token revoke error
             Log::error("Token revoke failed for user ".auth('api')->user()->id." message: ".$e->getMessage());

            return false;
        }

    }



}
