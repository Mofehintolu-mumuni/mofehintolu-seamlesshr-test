<?php

namespace  App\Http\Interfaces;

interface tokenManager
{

    function generateToken(string $email, string $password):array;

    function revokeToken();

}
