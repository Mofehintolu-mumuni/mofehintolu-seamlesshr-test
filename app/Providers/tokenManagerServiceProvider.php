<?php

namespace App\Providers;


use App\Http\Interfaces\tokenManager;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\API\AUTH\RegisterController;
use App\Http\Controllers\API\AUTH\LogoutController;
use App\Http\Controllers\API\AUTH\LoginController;
use App\Http\InterfaceImplementations\TokenManagerImplementations\jwtImplementation;

class tokenManagerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
       // inject the concrete class of the interface into the classes below

        $this->app->when(RegisterController::class)
        ->needs('App\Http\Interfaces\tokenManager')
        ->give(jwtImplementation::class);

        $this->app->when(LogoutController::class)
        ->needs('App\Http\Interfaces\tokenManager')
        ->give(jwtImplementation::class);


        $this->app->when(LoginController::class)
        ->needs('App\Http\Interfaces\tokenManager')
        ->give(jwtImplementation::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
