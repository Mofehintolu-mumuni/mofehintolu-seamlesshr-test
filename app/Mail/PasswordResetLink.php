<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PasswordResetLink extends Mailable
{
    use Queueable, SerializesModels;

    public $userEmail;
    public $resetToken;
    public $resetLink;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userEmail,$resetToken)
    {
        $this->userEmail = $userEmail;
        $this->resetToken = $resetToken;
        $this->resetLink = "localhost:8000/api/forgot/password/checkResetToken/{$resetToken}";

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Password Reset")
        ->markdown('emails.sendPasswordResetLink');

    }
}
