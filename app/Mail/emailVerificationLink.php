<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class emailVerificationLink extends Mailable
{
    use Queueable, SerializesModels;

    public $verificationToken;
    public $userEmail;
    public $verificationLink;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userEmail,$verificationToken)
    {
        $this->verificationToken = $verificationToken;
        $this->userEmail = $userEmail;
        $this->verificationLink = "localhost:8000/api/users/email/verification/{$verificationToken}";
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject("Email verification")
           ->markdown('emails.sendVerificationLink');
    }
}
