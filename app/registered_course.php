<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class registered_course extends Model
{
    public $timestamps = false;

    public function course()
    {
        return $this->belongsToMany('App\Course');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
