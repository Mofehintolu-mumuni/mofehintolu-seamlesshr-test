<?php

namespace App\Exceptions;

use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;


class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        if (($exception instanceof UnauthorizedHttpException) || ($exception instanceof InvalidArgumentException)) {

            $preException = $exception->getPrevious();

            if ($preException instanceof TokenExpiredException)
            {
                return response()->json(['error' => 'TOKEN IS EXPIRED'],400);
            }
            else if ($preException instanceof TokenInvalidException)
            {

                return response()->json(['error' => 'TOKEN IS INVALID'],400);
            }
            else if ($preException instanceof TokenBlacklistedException) {

                return response()->json(['error' => 'TOKEN IS BLACKLISTED'],400);
            }
        }




        if ($exception->getMessage() === 'Token not provided')
        {
            return response()->json(['error' => 'Token not provided'],400);
        }

        if ($exception->getMessage() === 'Route [login] not defined.')
        {
            return response()->json(['error' => 'unauthenticated'],400);
        }



        if ($exception instanceof TokenExpiredException)
        {
            return response()->json(['error' => 'TOKEN IS EXPIRED'],400);
        }
        else if ($exception instanceof TokenInvalidException)
        {

            return response()->json(['error' => 'TOKEN IS INVALID'],400);
        }
        else if ($exception instanceof TokenBlacklistedException) {

            return response()->json(['error' => 'TOKEN IS BLACKLISTED'],400);
        }
        else if ($exception instanceof JWTException) {

            return response()->json(['error' => 'TOKEN IS ABSENT'],400);
        }
        else if ($exception instanceof MethodNotAllowedHttpException) {

            return response()->json(['error' => 'ROUTE NOT VALID OR METHOD USED TO QUERY API IS NOT ALLOWED INFO: '.$exception->getMessage()],400);
        }


        return parent::render($request, $exception);
    }
}
