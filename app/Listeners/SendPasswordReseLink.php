<?php

namespace App\Listeners;

use App\Mail\PasswordResetLink;
use Illuminate\Support\Facades\Mail;
use App\Events\PasswordResetNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPasswordReseLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PasswordResetNotification  $event
     * @return void
     */
    public function handle(PasswordResetNotification $event)
    {

        $userEmail = $event->userEmail;
        $resetToken = $event->resetToken;

        //send mail to user
        Mail::to($userEmail)->send(new PasswordResetLink($userEmail,$resetToken));
    }
}
