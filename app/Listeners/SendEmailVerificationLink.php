<?php

namespace App\Listeners;

use App\Events\EmailVerification;
use Illuminate\Support\Facades\Mail;
use App\Mail\emailVerificationLink;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;



class SendEmailVerificationLink
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailVerification  $event
     * @return void
     */
    public function handle(EmailVerification $event)
    {

        $userEmail = $event->email;
        $verificationToken = $event->token;

        //send mail
        Mail::to($userEmail)->send(new emailVerificationLink($userEmail,$verificationToken));
    }
}
