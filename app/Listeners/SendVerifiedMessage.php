<?php

namespace App\Listeners;

use App\Events\VerifiedMessage;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerifiedAlert;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendVerifiedMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VerifiedMessage  $event
     * @return void
     */
    public function handle(VerifiedMessage $event)
    {
        $userEmail = $event->userEmail;
        //send welcome mail
        Mail::to($userEmail)->send(new EmailVerifiedAlert($userEmail));
    }
}
