@component('mail::message')
# Email verified alert

Hello your email has now been verified you can now enjoy all our services!

You are receiving this message because you created an account on Course Ware App with this email {{ $userEmail }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent


