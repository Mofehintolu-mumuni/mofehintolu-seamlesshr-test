

@component('mail::message')
# Password reset

Please use the button below to reset your password.

@component('mail::button', ['url' => $resetLink])
Reset password
@endcomponent

If you cannot use the button above copy and paste this link {{ $resetLink }} into your browser

You are receiving this message because you created an account on  Course Ware App with this email {{ $userEmail }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent


