@component('mail::message')
# Hello welcome to  Course Ware App

Please use the button below to verify your email

@component('mail::button', ['url' => $verificationLink])
Verify Email
@endcomponent

If you cannot use the button above copy and paste this link {{ $verificationLink }} into your browser

You are receiving this message because you created an account on  Course Ware App with this email {{ $userEmail }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent

